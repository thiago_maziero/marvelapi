package org.maz.marvelapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import org.maz.marvelapi.model.Hero;

public class HeroActivity extends Activity {

    public static final String HERO_ID_EXTRA = "hero_id_extra";

    private Hero mHero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hero_layout);

        Intent intent = getIntent();
        if (intent != null) {
//            mHeroId = intent.getStringExtra(HERO_ID_EXTRA);

        } else {
            Toast.makeText(this, "invalid hero", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
