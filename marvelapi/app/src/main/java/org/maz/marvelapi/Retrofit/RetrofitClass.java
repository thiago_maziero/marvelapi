package comoesz.example.douglaspanacho.retrofit.Retrofit;

import android.content.Context;

import comoesz.example.douglaspanacho.retrofit.Globals;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by douglaspanacho on 09/05/16.
 */
public class RetrofitClass {
    Context mContext;
    Retrofit mRetrofit;
    RequestsEndPoints.ApiEndPoints mApi;

    public RetrofitClass(Context mContext) {
        this.mContext = mContext;
        Retrofit.Builder retroBuilder = new Retrofit.Builder().baseUrl(Globals.URL).addConverterFactory(GsonConverterFactory.create());
        mRetrofit = retroBuilder.build();
        mApi = mRetrofit.create(RequestsEndPoints.ApiEndPoints.class);
    }

    public Retrofit getmRetrofit() {
        return mRetrofit;
    }

    public RequestsEndPoints.ApiEndPoints getApi() {
        return mApi;
    }
}
