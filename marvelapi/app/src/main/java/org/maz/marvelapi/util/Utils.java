package org.maz.marvelapi.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {
    private static final String API_KEY = "f78727db430c6174974786a899398007";
    private static final String API_PRIV_KEY = "fa4fc18ac16fbaea28f157f3b10d44e7ca1541d2";

    private static String getMd5(String string) {
        String hash = null;
        try {
            byte[] bytesOfMessage = string.getBytes();
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfMessage);
            BigInteger bigInt = new BigInteger(1, thedigest);
            hash = bigInt.toString(16);
            while (hash.length() < 32) {
                hash = "0" + hash;
            }
        } catch (NoSuchAlgorithmException e) {
        }
        return hash;
    }

    public static String addMd5ToUrl(String url) {
        long time = System.currentTimeMillis();
        String md5 = Utils.getMd5(time + API_PRIV_KEY + API_KEY);
        StringBuilder sb = new StringBuilder();
        sb.append(url)
                .append("apikey=")
                .append(API_KEY)
                .append("&ts=")
                .append(time)
                .append("&hash=")
                .append(md5);
        return sb.toString();
    }
}
