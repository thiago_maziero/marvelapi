package comoesz.example.douglaspanacho.retrofit.Retrofit;

import comoesz.example.douglaspanacho.retrofit.Response.CharacterDetailResponse;
import comoesz.example.douglaspanacho.retrofit.Response.CharactersResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by douglaspanacho on 09/05/16.
 */
public class RequestsEndPoints {

    public interface ApiEndPoints {

        @GET("characters")
        Call<CharactersResponse> getCharacters(@Query("ts") String ts,@Query("apikey") String apiKey,@Query("hash") String hash);

        @GET("characters/1009165")
        Call<CharacterDetailResponse> getCharactersDetail(@Query("ts") String ts,@Query("apikey") String apiKey,@Query("hash") String hash);

    }
}
