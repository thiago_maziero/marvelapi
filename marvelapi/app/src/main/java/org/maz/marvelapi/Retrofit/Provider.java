package comoesz.example.douglaspanacho.retrofit.Retrofit;

import android.content.Context;

import org.json.JSONException;

import comoesz.example.douglaspanacho.retrofit.Globals;
import comoesz.example.douglaspanacho.retrofit.Response.CharacterDetailResponse;
import comoesz.example.douglaspanacho.retrofit.Response.CharactersResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by douglaspanacho on 09/05/16.
 */
public class Provider<T> {

    Retrofit mRetrofit;
    RequestsEndPoints.ApiEndPoints mApi;
    Context mContext;

    public Provider(Context mContext) {
        this.mContext = mContext;
        RetrofitClass dataRequest = new RetrofitClass(mContext);
        mRetrofit = dataRequest.getmRetrofit();
        mApi = dataRequest.getApi();
    }

    public void getCharacters(Callback<CharactersResponse> handler) throws JSONException {
        Call<CharactersResponse> call = mApi.getCharacters(Globals.TS,Globals.API_KEY,Globals.Hash);
        call.enqueue(handler);
    }

    public void getCharactersDetails(Callback<CharacterDetailResponse> handler) throws JSONException {
        Call<CharacterDetailResponse> call = mApi.getCharactersDetail(Globals.TS,Globals.API_KEY,Globals.Hash);
        call.enqueue(handler);
    }


}
