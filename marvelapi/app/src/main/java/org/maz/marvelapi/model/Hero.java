package org.maz.marvelapi.model;

import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class Hero {
    private String mName;
    private String mImageUrl;
    private String mDesc;

    public Hero(JSONObject object) throws JSONException {
        this.mName = object.get("name").toString();
        this.mImageUrl = object.getJSONObject("thumbnail").get("path").toString();
        this.mDesc = object.get("description").toString();
    }

    public String getName() {
        return mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getDesc() {
        return mDesc;
    }

    @Override
    public String toString() {
        return mName;
    }
}
