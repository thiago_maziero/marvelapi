package comoesz.example.douglaspanacho.retrofit.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by douglaspanacho on 05/09/16.
 */
public class CharacterDetailResponse {


        private Integer code;
        private String status;
        private String etag;
        private Data data;

        /**
         * @return The code
         */
        public Integer getCode() {
            return code;
        }

        /**
         * @param code The code
         */
        public void setCode(Integer code) {
            this.code = code;
        }

        /**
         * @return The status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * @return The etag
         */
        public String getEtag() {
            return etag;
        }

        /**
         * @param etag The etag
         */
        public void setEtag(String etag) {
            this.etag = etag;
        }

        /**
         * @return The data
         */
        public Data getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(Data data) {
            this.data = data;
        }


        public class Data {

            private Integer offset;
            private Integer limit;
            private Integer total;
            private Integer count;
            private List<Result> results = new ArrayList<Result>();

            /**
             * @return The offset
             */
            public Integer getOffset() {
                return offset;
            }

            /**
             * @param offset The offset
             */
            public void setOffset(Integer offset) {
                this.offset = offset;
            }

            /**
             * @return The limit
             */
            public Integer getLimit() {
                return limit;
            }

            /**
             * @param limit The limit
             */
            public void setLimit(Integer limit) {
                this.limit = limit;
            }

            /**
             * @return The total
             */
            public Integer getTotal() {
                return total;
            }

            /**
             * @param total The total
             */
            public void setTotal(Integer total) {
                this.total = total;
            }

            /**
             * @return The count
             */
            public Integer getCount() {
                return count;
            }

            /**
             * @param count The count
             */
            public void setCount(Integer count) {
                this.count = count;
            }

            /**
             * @return The results
             */
            public List<Result> getResults() {
                return results;
            }

            /**
             * @param results The results
             */
            public void setResults(List<Result> results) {
                this.results = results;
            }

        }



        public class Result {

            private Integer id;
            private String name;
            private String description;

            /**
             * @return The id
             */
            public Integer getId() {
                return id;
            }

            /**
             * @param id The id
             */
            public void setId(Integer id) {
                this.id = id;
            }

            /**
             * @return The name
             */
            public String getName() {
                return name;
            }

            /**
             * @param name The name
             */
            public void setName(String name) {
                this.name = name;
            }

            /**
             * @return The description
             */
            public String getDescription() {
                return description;
            }

            /**
             * @param description The description
             */
            public void setDescription(String description) {
                this.description = description;
            }

        }


}
